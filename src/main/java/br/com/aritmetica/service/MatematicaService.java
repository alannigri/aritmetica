package br.com.aritmetica.service;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entradaDTO) {
        int numero = 0;
        for (int i : entradaDTO.getNumeros()) {
            numero += i;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO subtracao(EntradaDTO entradaDTO) {
        int primeiro = entradaDTO.getNumeros().get(0);
        int resultado = primeiro;
        for (int i = 1; i < entradaDTO.getNumeros().size(); i++) {
            resultado -= entradaDTO.getNumeros().get(i);
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(resultado);
        return resposta;
    }

    public RespostaDTO multiplicacao(EntradaDTO entradaDTO) {
        int primeiro = entradaDTO.getNumeros().get(0);
        int numero = primeiro;
        for (int i = 1; i < entradaDTO.getNumeros().size(); i++) {
            numero = numero * entradaDTO.getNumeros().get(i);
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO divisao(EntradaDTO entradaDTO) {
        int primeiro = entradaDTO.getNumeros().get(0);
        int ultimo = entradaDTO.getNumeros().get(1);
        int resultado = primeiro / ultimo;
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(resultado);
        return resposta;
    }
}

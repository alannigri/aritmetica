package br.com.aritmetica.controllers;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import br.com.aritmetica.service.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/math")
public class MatematicaController {

    @GetMapping
    public String olaMundo() {

        return "Olá Mundo!";
    }

    @Autowired
    private MatematicaService matematicaService;

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entrada) {
        if (entrada.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie mais de 1 numero para somar");
        } else if (!entrada.validaNumeros(entrada.getNumeros())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie apenas numeros maiores que zero");
        }
        RespostaDTO resposta = matematicaService.soma(entrada);

        return resposta;
    }

    @PutMapping("/subtracao")
    public RespostaDTO subtracao(@RequestBody EntradaDTO entrada) {
        if (entrada.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie mais de 1 numero para subtrair");
        } else if (!entrada.validaNumeros(entrada.getNumeros())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie apenas numeros maiores que zero");
        }
        RespostaDTO resposta = matematicaService.subtracao(entrada);

        return resposta;
    }

    @PutMapping("/multiplicacao")
    public RespostaDTO multiplicacao(@RequestBody EntradaDTO entrada) {
        if (entrada.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie mais de 1 numero para multiplicar");
        } else if (!entrada.validaNumeros(entrada.getNumeros())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie apenas numeros maiores que zero");
        }
        RespostaDTO resposta = matematicaService.multiplicacao(entrada);

        return resposta;
    }

    @PutMapping("/divisao")
    public RespostaDTO divisao(@RequestBody EntradaDTO entrada) {
        if (entrada.getNumeros().size() != 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie apenas 2 numeros para divisão");
        } else if (!entrada.validaNumeros(entrada.getNumeros())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie apenas numeros maiores que zero");
            } else if (entrada.getNumeros().get(0) < entrada.getNumeros().get(1)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Primeiro numero deve ser maior que o segundo");
        }
        RespostaDTO resposta = matematicaService.divisao(entrada);
        return resposta;
    }
}

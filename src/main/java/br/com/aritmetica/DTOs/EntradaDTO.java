package br.com.aritmetica.DTOs;

import java.util.List;

public class EntradaDTO {
    private List<Integer> numeros;

    public EntradaDTO() {
    }

    public List<Integer> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }

    public boolean validaNumeros(List<Integer> numeros){
        for(int i =0; i< getNumeros().size(); i++){
            if (getNumeros().get(i) <=0){
                return false;
            }
        }
        return true;
    }
}
